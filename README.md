# Pacman 3D

A 3D remake of the classic game Pacman.

# Required libraries

Stb, Glm, and Glad are provided within the project (include directory).
Compilation requires OpenGL and Assimp installed on the system.

# Models and textures

Models and textures for this game were created using [Blender](https://www.blender.org/) and [GIMP](https://www.gimp.org/).

Model for the board was created by [Michał Miłek](https://gitlab.com/mmilek).

Other models and all textures were created by [Artur Gulik](https://gitlab.com/ArturGulik), the project owner.

# Keybinds
- W, S, A, D - Pacman movement
- T, G, F, H - Camera movement (only F and H in pacman follow mode)
- R, Y - Camera height
- Arrow keys - Camera rotation
- C - Switch camera mode
- X - Exit the game
