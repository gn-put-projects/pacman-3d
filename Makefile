MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

# Paths for source and object files
S=src
O=$S/obj

# Compiler and compiler flags
CC=g++
CFLAGS=-std=c++20 -I./include -Wall -g
OBJ_CFLAGS=-c $(CFLAGS)

# Used libraries
#LIBOPENGL=-lGL -lglfw -lGLEW
LIBOPENGL=-lGL -lglfw -lGLEW 
LIBASSIMP=-lassimp

LIBS=$(LIBOPENGL) $(LIBASSIMP)

.PHONY: test compile clean hardcore h2
test: pacman-3d
	./pacman-3d

compile: pacman-3d

pacman-3d: $O/main.o $O/App.o $O/Game.o $O/Board.o $O/Pacman.o $O/Model.o $O/Mesh.o $O/glad.o $O/Shader.o $O/Camera.o $O/Ghost.o $O/Food.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIBS)

$O/glad.o: $S/glad.c
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/main.o: $S/main.cpp $S/App.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/App.o: $S/App.cpp $S/App.h $S/Game.cpp $S/Game.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Board.o: $S/Board.cpp $S/Board.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Game.o: $S/Game.cpp $S/Game.h $S/Board.cpp $S/Board.h $S/Pacman.cpp $S/Pacman.h $S/Ghost.cpp $S/Ghost.h 
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Pacman.o: $S/Pacman.cpp $S/Pacman.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Shader.o: $S/Shader.cpp $S/Shader.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Mesh.o: $S/Mesh.cpp $S/Mesh.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Model.o: $S/Model.cpp $S/Model.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Camera.o: $S/Camera.cpp $S/Camera.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Ghost.o: $S/Ghost.cpp $S/Ghost.h $S/Game.cpp $S/Game.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/Food.o: $S/Food.cpp $S/Food.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

hardcore:
	g++ $(CFLAGS) $(LIBS) src/*.cpp src/*.c -o pacman-3d

h2:
	g++ $(CFLAGS) $(LIBS) src/main.cpp src/App.cpp src/*.c -o pacman-3d


clean:
	rm -f pacman-3d $O/*.o
