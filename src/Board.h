#ifndef BOARD_H
#define BOARD_H

#include <cstdio>
#include <iostream>
#include <string>

//#include <GLFW/glfw3.h>

#include "Shader.h"
#include "Model.h"

#define BOARD_WIDTH 21
#define BOARD_HEIGHT 21

using std::string;

class Board {
 private:
  int rows, columns;
  int GHEntranceX, GHEntranceY;
  int b[21][21];
  bool aborting; // value set to true in order to exit Board.loop()
  string abrt_msg; // abort information
  Model* model;
 public:
  void draw(Shader*);
  void drawTerm();
  bool wall(int, int);
  int getBoardValue(int, int);
  Board();
  ~Board();
  int getWidth();
  int getHeight();
  int getGHEntranceX();
  int getGHEntranceY();
};

#endif
