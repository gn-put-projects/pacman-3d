#include "Mesh.h"

Mesh::Mesh(vector<Vertex> vertices, vector<uint> indices,
	   vector<Texture> textures) {
  this->vertices = vertices;
  this->indices = indices;
  this->textures = textures;

  this->transform = glm::mat4(1.0f);
  
  glGenVertexArrays(1, &this->VAO);
  glGenBuffers(1, &this->VBO);
  glGenBuffers(1, &this->EBO);

  glBindVertexArray(this->VAO);
  
  glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
  glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(Vertex),
	       &vertices[0], GL_STATIC_DRAW);
  
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size()*sizeof(uint),
	       &indices[0], GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
  glEnableVertexAttribArray(0);
  // vertex normals
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
  glEnableVertexAttribArray(1);
  
  // vertex texture coords
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
  glEnableVertexAttribArray(2);

  glBindVertexArray(0);
  
}

Mesh::~Mesh() {

}

void Mesh::draw(Shader* shader, glm::mat4& M) {
  uint diffuse = 0; //index of current diffuse texture used
  uint specular = 0; //index of current specular texture used
  for (uint i=0; i < this->textures.size(); ++i) {
    string shaderIntName; //pattern: material.<texture.name><index>
    glActiveTexture(GL_TEXTURE0 + i);
    if (textures[i].type == "texture_diffuse") {
      diffuse++;
      shaderIntName = "material." + textures[i].type +
	std::to_string(diffuse);
    }
    else if (textures[i].type == "texture_specular") {
      specular++;
      shaderIntName = "material." + textures[i].type +
	std::to_string(specular);
    }
    else {
      fprintf(stderr, "Fatal error: unknown texture type (must be texture_diffuse or texture_specular)");
      return;
    }
    shader->setValue(shaderIntName.c_str(), i);
    glBindTexture(GL_TEXTURE_2D, textures[i].id);
  }
  glActiveTexture(GL_TEXTURE0);

  M = M * this->transform;
  glUniformMatrix4fv(shader->uni("M"), 1, false, glm::value_ptr(M));
  
  glBindVertexArray(this->VAO);
  glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
  glBindVertexArray(0);
}

void Mesh::drawArray(Shader* shader, std::unordered_map<int, glm::mat4>* Ms) {
  uint diffuse = 0; //index of current diffuse texture used
  uint specular = 0; //index of current specular texture used
  for (uint i=0; i < this->textures.size(); ++i) {
    string shaderIntName; //pattern: material.<texture.name><index>
    glActiveTexture(GL_TEXTURE0 + i);
    if (textures[i].type == "texture_diffuse") {
      diffuse++;
      shaderIntName = "material." + textures[i].type +
	std::to_string(diffuse);
    }
    else if (textures[i].type == "texture_specular") {
      specular++;
      shaderIntName = "material." + textures[i].type +
	std::to_string(specular);
    }
    else {
      fprintf(stderr, "Fatal error: unknown texture type (must be texture_diffuse or texture_specular)");
      return;
    }
    shader->setValue(shaderIntName.c_str(), i);
    glBindTexture(GL_TEXTURE_2D, textures[i].id);
  }
  glActiveTexture(GL_TEXTURE0);

  for (const auto& n : *Ms) {
    glUniformMatrix4fv(shader->uni("M"), 1, false, glm::value_ptr(n.second));
  
    glBindVertexArray(this->VAO);
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
  }
  glBindVertexArray(0);
}
