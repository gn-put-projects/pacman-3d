#version 330

out vec4 pixelColor;

in vec4 i_color;
in vec2 texCoordV;
in vec4 norm;

uniform vec3 viewPos;
uniform sampler2D texture2D;
uniform vec4 lightDir=vec4(20,10,-12,0);
uniform vec4 lightDir2=vec4(-10,10,12,0);

float kd = 0.75;
float ks = 1 - kd;
float k = 1;
float pi = 3.14;

in vec3 FragPos;

vec4 l = normalize(lightDir); // wektor od swiatla
vec4 l2 = normalize(lightDir2); // wektor od swiatla
vec4 n = normalize(norm); // wektor normalny
vec4 r = normalize(2*(n*l)*n-l); //wektor odbity
vec4 r2 = normalize(2*(n*l2)*n-l2); //wektor odbity
vec4 v = normalize(vec4(viewPos,1));// wektor do obserwatora
vec4 h = normalize(l+v / abs(l+v)); //wektor w pol drogi 
void main(void) {

     //float const = 1;
     //float phong = kd/pi;
     float phong = kd*dot(n.xyz, l.xyz)+ ks*pow(dot(v.xyz, r.xyz),k);//model phonga
     //float phong = kd/pi + ks*(k+2)/(2*pi)*pow(dot(n.xyz,h.xyz),k); //model phonga blinna poprawny fizycznie gdy kd + ks <= 1
     
     float phong2 = kd*dot(n.xyz, l2.xyz)+ ks*pow(dot(v.xyz, r2.xyz),k);//model phonga

     float ambientStrength = 0.25;

     phong = clamp(phong,0,0.5);
     phong2 = clamp(phong2,0,0.5);

     pixelColor=vec4(texture(texture2D, texCoordV).rgb*((phong+phong2)+ambientStrength), 1);
}