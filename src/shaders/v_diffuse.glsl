#version 330

uniform mat4 P;
uniform mat4 V;
uniform mat4 M;

uniform vec4 color=vec4(1,0,0,1);
//uniform vec4 lightDir=vec4(0,0,1,0);


layout (location=0) in vec4 vertex;
layout (location=1) in vec4 normal;
layout (location=2) in vec2 texCoords;

out vec4 i_color;
out vec2 texCoordV;
out vec4 norm;

out vec3 FragPos;

void main(void) {
    gl_Position=P*V*M*vertex;

    mat4 G=mat4(inverse(transpose(mat3(M))));
    vec4 n=normalize(V*G*normal);

    float nl;//=clamp(dot(n,lightDir),0.5,1);

    if (normal.x < 0.5)
       nl = 0;
    else nl = 1;
    nl=(normal.x+normal.y+normal.z)/1.2;

    i_color=vec4(color.rgb*nl,color.a);
    texCoordV=texCoords;

    FragPos = vec3(M * vertex);
    norm = normalize(G*normal);
}