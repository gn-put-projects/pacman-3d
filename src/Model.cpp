#include "Model.h"

using namespace std;

Model::Model(const string& path, bool gamma, string textureOverloadStr) {
  this->gammaCorrection = gamma;
  this->textureOverload = textureOverloadStr;
  this->init(path);
}

Model::Model(const string& path, bool gamma) {
  this->gammaCorrection = gamma;
  this->textureOverload = "";
  this->init(path);
}

void Model::init(const string& path) {
  this->src = path;
  this->success = true;
  this->failureMsg = "";
  this->transform = glm::mat4(1.0f);
  this->scale = glm::vec3(1,1,1);
  this->translate = glm::vec3(0,0,0);
  this->loadModel(path);
}

void Model::draw(Shader* shader) {
  glm::mat4 M;
  for(uint i=0; i<this->meshes.size(); i++) {
    M = glm::mat4(1.0f);
    M = glm::translate(M, this->translate);
    M = glm::scale(M, this->scale);
    M = M * this->transform;
    this->meshes[i].draw(shader, M);
  }
}

void Model::drawArray(Shader* shader, std::unordered_map<int, glm::mat4>* Ms) {
  for(uint i=0; i<this->meshes.size(); i++) {
    this->meshes[i].drawArray(shader, Ms);
  }
}
    
void Model::loadModel(const string& path) {
  Assimp::Importer importer;
  const aiScene* scene = importer.ReadFile
    (path, aiProcess_Triangulate | aiProcess_GenSmoothNormals |
     aiProcess_FlipUVs | aiProcess_CalcTangentSpace);
  
  if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
    this->success = false;
    this->failureMsg = "Error importing model with Assimp: " +
      string(importer.GetErrorString());
    return;
  }
  directory = path.substr(0, path.find_last_of('/'));
  this->processNode(scene->mRootNode, scene);
}

void Model::processNode(aiNode* node, const aiScene* scene) {
  for(uint i=0; i<node->mNumMeshes; ++i) {
    aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
    this->meshes.push_back(this->processMesh(mesh, scene));
  }
  for(uint i=0; i<node->mNumChildren; ++i) {
    this->processNode(node->mChildren[i], scene);
  }
}

Mesh Model::processMesh(aiMesh* mesh, const aiScene* scene) {
  vector<Vertex> vertices;
  vector<uint> indices;
  vector<Texture> textures;

  for(uint i=0; i<mesh->mNumVertices; ++i) {
    Vertex vertex;
    glm::vec3 vector;
            
    vector.x = mesh->mVertices[i].x;
    vector.y = mesh->mVertices[i].y;
    vector.z = mesh->mVertices[i].z;
    vertex.Position = vector;
    
    if (mesh->HasNormals()) {
      vector.x = mesh->mNormals[i].x;
      vector.y = mesh->mNormals[i].y;
      vector.z = mesh->mNormals[i].z;
      vertex.Normal = vector;
    }

    if(mesh->mTextureCoords[0]) {
      glm::vec2 vec;
      vec.x = mesh->mTextureCoords[0][i].x; 
      vec.y = mesh->mTextureCoords[0][i].y;
      vertex.TexCoords = vec;
      // tangent
      vector.x = mesh->mTangents[i].x;
      vector.y = mesh->mTangents[i].y;
      vector.z = mesh->mTangents[i].z;
      vertex.Tangent = vector;
      // bitangent
      vector.x = mesh->mBitangents[i].x;
      vector.y = mesh->mBitangents[i].y;
      vector.z = mesh->mBitangents[i].z;
      vertex.Bitangent = vector;
    }
    else
      vertex.TexCoords = glm::vec2(0.0f, 0.0f);
    
    vertices.push_back(vertex);
  }

  //loop over mesh faces (triangles)
  for(uint i=0; i<mesh->mNumFaces; ++i) {
    aiFace face = mesh->mFaces[i];
    
    for(uint j=0; j<face.mNumIndices; ++j)
      indices.push_back(face.mIndices[j]);        
  }
  aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];    
  
  // 1. diffuse maps
  vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
  textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
  
  // 2. specular maps
  vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
  textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

  // 3. normal maps
  std::vector<Texture> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
  textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());

  // 4. height maps
  std::vector<Texture> heightMaps = loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
  textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
        
  return Mesh(vertices, indices, textures);
}

vector<Texture> Model::loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName) {
  vector<Texture> textures;
  for(uint i=0; i<mat->GetTextureCount(type); ++i) {
    aiString str;
    if (this->textureOverload == "")
      mat->GetTexture(type, i, &str);
    else str = this->textureOverload;
    bool loaded = false;
    for(uint j=0; j<this->textures_loaded.size(); ++j) {
      if(std::strcmp(this->textures_loaded[j].path.data(), str.C_Str()) == 0) {
	textures.push_back(this->textures_loaded[j]);
	loaded = true;
	break;
      }
    }
    if (!loaded) {
      Texture texture;
      texture.id = TextureFromFile(str.C_Str(), this->directory, false);
      texture.type = typeName;
      texture.path = str.C_Str();
      textures.push_back(texture);
      this->textures_loaded.push_back(texture);
    }
  }
  return textures;
}

uint Model::TextureFromFile(const char* path, const string& directory, bool gamma) {
  string filename = string(path);
  filename = directory + '/' + filename;

  uint textureID;
  glGenTextures(1, &textureID);

  int width, height, nrComponents;
  
  unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
  if (!data) {
    this->success = false;
    this->failureMsg = "Error loading texture from: " + string(path);
    stbi_image_free(data);
    return textureID;
  }
  
  GLenum format = GL_RED;
  if (nrComponents == 1)
    format = GL_RED;
  else if (nrComponents == 3)
    format = GL_RGB;
  else if (nrComponents == 4)
    format = GL_RGBA;
  
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);
  
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  
  stbi_image_free(data);

  return textureID;
}

Model::~Model() {
}
