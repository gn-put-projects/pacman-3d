#include "Camera.h"

void logVec3(glm::vec3& data) {
  std::cout<<data.x<<" "<<data.y<<" "<<data.z<<"\n";
}

Camera::Camera(Game* game) {
  this->game = game;
  this->pos = glm::vec3(0,100,0);
  this->prevPos = glm::vec3(0,100,0);
  this->dir = glm::vec3(0, 0, 1);
  
  this->speed = glm::vec3(0.0f, 0.0f, 0.0f);
  this->followHeightSpeed = 0.0f;
  this->followDistanceSpeed = 0.0f;

  this->rotspeed = glm::vec3(0, 0, 0);

  this->yaw = -90.0f;
  this->pitch = -90.0f;

  this->followDistance = FOLLOW_DISTANCE;
  this->followHeight = FOLLOW_HEIGHT;

  this->mode = CAM_FOLLOW;
}

Camera::~Camera() {

}

void Camera::follow(glm::vec3 objectPos, float angle) {
  this->pos = objectPos +
    glm::vec3(-this->followDistance*cos(glm::radians(angle)),
	      this->followHeight,
	      this->followDistance*sin(glm::radians(angle)));
  this->dir = glm::vec3(0, 2, 0);
}

void Camera::update(double time) {
  if (this->mode == CAM_FOLLOW) {
    this->followDistance += this->followDistanceSpeed * time;
    this->followHeight += this->followHeightSpeed * time;
    this->follow(this->game->getPacman()->pos,
		 this->game->getPacman()->yaw);
    this->lookAt = this->game->getPacman()->pos + glm::vec3(0, 2, 0);
    return;
  }
  this->yaw += this->rotspeed.x * time;
  this->pitch += this->rotspeed.y * time;
  
  if(pitch > 89.0f)
    pitch =  89.0f;
  if(pitch < -89.0f)
    pitch = -89.0f;

  glm::vec3 direction;
  direction.x = cos(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
  direction.y = sin(glm::radians(this->pitch));
  direction.z = sin(glm::radians(this->yaw)) * cos(glm::radians(this->pitch));
  this->dir = glm::normalize(direction);

  this->pos.y += this->speed.y * time;

  this->pos += float(time) * this->speed.z * this->dir +
    float(time) *
    glm::normalize(glm::cross(this->dir, glm::vec3(0.0f,1.0f,0.0f))) * this->speed.x;

  this->lookAt = this->pos + this->dir;
}

void Camera::move(int key) {
  if (key == MODE_CHANGE) {
    this->speed = glm::vec3(0.0f, 0.0f, 0.0f);
    this->followHeightSpeed = 0.0f;
    this->followDistanceSpeed = 0.0f;
    if (this->mode == CAM_FREE) {
      this->mode = CAM_FOLLOW;
      this->prevPos = this->pos;
    }
    else {
      this->mode = CAM_FREE;
      this->pos = this->prevPos;
    }
    return;
  }
  if (key == CAMERA_LEFT) {
    this->speed.x -= CAMERA_SPEED_X;
    return;
  }
  if (key == CAMERA_RIGHT) {
    this->speed.x += CAMERA_SPEED_X;
    return;
  }
  if (key == CAMERA_FORWARD) {
    if (this->mode == CAM_FREE)
      this->speed.z += CAMERA_SPEED_Z;
    else
      this->followDistanceSpeed -= FOLLOW_DISTANCE_SPEED;
    return;
  }
  if (key == CAMERA_BACKWARD) {
    if (this->mode == CAM_FREE)
      this->speed.z -= CAMERA_SPEED_Z;
    else
      this->followDistanceSpeed += FOLLOW_DISTANCE_SPEED;
    return;
  }
  if (key == CAMERA_UP) {
    if (this->mode == CAM_FREE)
      this->speed.y += CAMERA_SPEED_Y;
    else
      this->followHeightSpeed += FOLLOW_HEIGHT_SPEED;
    return;
  }
  if (key == CAMERA_DOWN) {
    if (this->mode == CAM_FREE)
      this->speed.y -= CAMERA_SPEED_Y;
    else this->followHeightSpeed -= FOLLOW_HEIGHT_SPEED;
    return;
  }
  if (key == CAMERA_PITCH_UP) {
    this->rotspeed.y += CAMERA_PITCH_SPEED;
    return;
  }
  if (key == CAMERA_PITCH_DOWN) {
    this->rotspeed.y -= CAMERA_PITCH_SPEED;
    return;
  }
  if (key == CAMERA_YAW_LEFT) {
    this->rotspeed.x -= CAMERA_YAW_SPEED;
    return;
  }
  if (key == CAMERA_YAW_RIGHT) {
    this->rotspeed.x += CAMERA_YAW_SPEED;
    return;
  }
}

void Camera::stopMove(int key) {
  if (key == CAMERA_LEFT) {
    this->speed.x += CAMERA_SPEED_X;
    return;
  }
  if (key == CAMERA_RIGHT) {
    this->speed.x -= CAMERA_SPEED_X;
    return;
  }
  if (key == CAMERA_FORWARD) {
    if (this->mode == CAM_FREE)
      this->speed.z -= CAMERA_SPEED_Z;
    else
      this->followDistanceSpeed += FOLLOW_DISTANCE_SPEED;
    return;
  }
  if (key == CAMERA_BACKWARD) {
    if (this->mode == CAM_FREE)
      this->speed.z += CAMERA_SPEED_Z;
    else
      this->followDistanceSpeed -= FOLLOW_DISTANCE_SPEED;
    return;
  }
  if (key == CAMERA_UP) {
    if (this->mode == CAM_FREE)
      this->speed.y -= CAMERA_SPEED_Y;
    else this->followHeightSpeed -= FOLLOW_HEIGHT_SPEED;
    return;
  }
  if (key == CAMERA_DOWN) {
    if (this->mode == CAM_FREE)
      this->speed.y += CAMERA_SPEED_Y;
    else this->followHeightSpeed += FOLLOW_HEIGHT_SPEED;
    return;
  }
  if (key == CAMERA_PITCH_UP) {
    this->rotspeed.y -= CAMERA_PITCH_SPEED;
    return;
  }
  if (key == CAMERA_PITCH_DOWN) {
    this->rotspeed.y += CAMERA_PITCH_SPEED;
    return;
  }
  if (key == CAMERA_YAW_LEFT) {
    this->rotspeed.x += CAMERA_YAW_SPEED;
    return;
  }
  if (key == CAMERA_YAW_RIGHT) {
    this->rotspeed.x -= CAMERA_YAW_SPEED;
    return;
  }
}
