#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

using std::string;
typedef unsigned int uint;
typedef const char* cstr;

class Shader;

class Shader {
private:
public:
  uint id;
  bool error;
  void use();
  GLuint uni(const string&);
  GLuint atr(const string&);
  void setValue(const string &name, int value);
  Shader(cstr, cstr);
  ~Shader();
};

#endif
