#ifndef GAME_H
#define GAME_H

#include <cstdio>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Game;

#include "Board.h"
#include "Pacman.h"
#include "Shader.h"
#include "Camera.h"
#include "Ghost.h"
#include "Food.h"

using std::string;
using std::vector;

class Game {
 private:
  GLFWwindow* window;
  Board* board;
  Pacman* pacman;
  Camera* camera;
  Food* food;
  vector<glm::vec2> foods;
  Ghost* blinky;
  Ghost* pinky;
  Ghost* inky;
  Ghost* clyde;

public:
  int viewMode;
  bool success;
  string failureMsg;
  void update(double);
  void render(Shader* shader);
  void drawTerm();
  void keyDown(int);
  void keyUp(int);
  void eat(int, int);
  bool wall(int, int);
  Board* getBoard();
  Game(GLFWwindow*);
  ~Game();
  Pacman* getPacman();
  Ghost* getBlinky();
};

#endif
