#ifndef MODEL_H
#define MODEL_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <unordered_map>

#include <glad/glad.h> 
#include <glm/glm.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#if !defined PACMAN_H && !defined BOARD_H
#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>
#endif

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class Model;

#include "Mesh.h"
#include "Shader.h"

typedef unsigned int uint;

using std::string;
using std::vector;

class Model {
private:
  string src;
  void loadModel(const string&);
  void processNode(aiNode*, const aiScene*);
  Mesh processMesh(aiMesh*, const aiScene*);
  vector<Texture> loadMaterialTextures(aiMaterial*, aiTextureType, string);
  uint TextureFromFile(const char*, const string&, bool);
  string textureOverload;
  public:
  bool success;
  string failureMsg;
  glm::mat4 transform;
  glm::vec3 scale;
  glm::vec3 translate;
  vector<Texture> textures_loaded;
  vector<Mesh> meshes;
  string directory;
  bool gammaCorrection;

  void draw(Shader*);
  void drawArray(Shader*, std::unordered_map<int, glm::mat4>*);
  Model(const string&, bool, string);
  Model(const string&, bool);
  void init(const string&);
  ~Model();
};

#endif
