#include "Pacman.h"

const std::unordered_map<int, float> facingAngle = {
  {DIRECTION_UP, 90.0f},
  {DIRECTION_DOWN, -90.0f},
  {DIRECTION_LEFT, 180.0f},
  {DIRECTION_RIGHT, 0.0f}
};

double posmod(float l, float r) {
  int full = int(l)/int(r);
  return l - full*int(r);
}

int sign(float x) {
  if (x<0) return -1;
  return 1;
}

float absf(float x) {
  return (x<0)? -x : x;
}

int rotateRight(int direction) {
  return (direction%2)? direction*(-2) : direction/2;
}

int rotateLeft(int direction) {
  return (direction%2)? direction*2 : direction/(-2);
}

int getXMultiplier(int direction) {
  return direction/2;
}

int getYMultiplier(int direction) {
  return direction%2;
}

int roundPosition (float x, int dir) {
  //dir specifies whether the value is rising (1) or falling (-1)
  if (dir<0)
    return int(x/(MOVE_UNIT/2) + 1)*MOVE_UNIT/2;
  return int(x/(MOVE_UNIT/2))*MOVE_UNIT/2;
}

int getRoundedPosition (float x, int dir) {
  if (dir<0)
    return roundPosition(x,dir)-50;
  return roundPosition(x,dir);
}

int Pacman::nextX(double change, int dir, bool hardCheck=false) {
  return (this->x + getXMultiplier(dir)*
	  (change + ((hardCheck)?(3*MOVE_UNIT/4) : (MOVE_UNIT/2)))
	  )/MOVE_UNIT;
}

int Pacman::nextY(double change, int dir, bool hardCheck=false) {
  return (this->y + getYMultiplier(dir)*
	  (change + ((hardCheck)?(3*MOVE_UNIT/4) : (MOVE_UNIT/2)))
	  )/MOVE_UNIT;
}

void Pacman::manageTurnRequest(double change) {
  if (this->direction == this->newDirection)
    return;
  
  int X = this->nextX(change, this->newDirection, true);
  int Y = this->nextY(change, this->newDirection, true);
  if (!this->game->wall(X,Y)){
    //std::cout<<"No wall at: ("<<X<<", "<<Y<<"). Turning!\n";
    this->direction = this->newDirection;
    return;
  }
  // Failed to change direction
  //this->newDirection = this->direction;
}

Pacman::Pacman(Game* game) {
  this->game = game;
  this->model = new Model("models/pacman.obj", false);
  this->success = this->model->success;
  this->failureMsg = "Pacman: " + this->model->failureMsg;
  this->direction = DIRECTION_LEFT;
  this->newDirection = DIRECTION_LEFT;
  this->yaw = facingAngle.at(this->direction);
  this->x = PACMAN_STARTPOS_X*MOVE_UNIT + 50.0;
  this->y = PACMAN_STARTPOS_Y*MOVE_UNIT + 50.0;
  this->speed = 3*MOVE_UNIT;
  this->mouthAngle = 0.0f;
  this->mouthAction = MOUTH_OPEN;
  this->mouthSpeed = MOUTH_SPEED;
}

Pacman::~Pacman() {
  delete this->model;
}

void Pacman::moveMouth(double time) {
  double change = this->mouthSpeed * time;
  if (change >= MAX_MOUTH_OPEN_ANGLE/4) {
    printf("Mouth too laggy!\n");
    return;
  }
  this->mouthAngle += change * this->mouthAction;
  
  if (this->mouthAngle > MAX_MOUTH_OPEN_ANGLE/2) {
    this->mouthAngle = MAX_MOUTH_OPEN_ANGLE/2;
    this->mouthAction = MOUTH_CLOSE;
  }
  if (this->mouthAngle < 0) {
    this->mouthAngle = 0;
    this->mouthAction = MOUTH_OPEN;
  }
}

void Pacman::update(double time) {
  this->moveMouth(time);
  double change = this->speed * time;
  if (change >= MOVE_UNIT/4) {
    printf("Too laggy!\n");
    return;
  }

  int dx = getXMultiplier(this->direction);
  int dy = getYMultiplier(this->direction);

  double newX = this->x + change * dx;
  double newY = this->y + change * dy;

  //std::cout<<"Pacman pos: ["<<this->x<<", "<<this->y<<"]"<<
  //  " dir: "<<this->direction<<" ddir: "<<this->newDirection<<
  //  " delta: "<<change<<"\n";
  
  // If player requested a U-turn
  if (this->direction == -1 * this->newDirection){
    // Changing direction (no movement this tick)
    this->direction = this->newDirection;
    return;
  }

  bool passingX = false;
  bool passingY = false;
  
  int currentRoundedX = roundPosition(this->x, dx);
  int upcomingRoundedX = roundPosition(newX, dx);
  int currentRoundedY = roundPosition(this->y, dy);
  int upcomingRoundedY = roundPosition(newY, dy);

  // passing a crossroad on the x-axis
  passingX = (currentRoundedX != upcomingRoundedX &&
	      upcomingRoundedX % 100 != 0);
  // passing a crossroadc on the y-axis
  passingY = (currentRoundedY != upcomingRoundedY &&
	      upcomingRoundedY % 100 != 0);

  if (passingX) {
    this->x = roundPosition(newX, dx);
    change = absf(this->x - newX);
    //std::cout<<currentRoundedX<<" X "<<upcomingRoundedX<<"\n";
  }
  if (passingY) {
    this->y = roundPosition(newY, dy);
    change = absf(this->y - newY);
    //std::cout<<currentRoundedY<<" Y "<<upcomingRoundedY<<"\n";
  }
  if (passingX || passingY) {
    this->game->eat(this->x/MOVE_UNIT, this->y/MOVE_UNIT);
    this->manageTurnRequest(change);   
  }

  this->adjustYaw(time, facingAngle.at(this->direction));
  
  int X = this->nextX(change, this->direction);
  int Y = this->nextY(change, this->direction);
  if (this->game->wall(X, Y)){
    if (getXMultiplier(this->direction)) this->x = getRoundedPosition(this->x, this->direction);
    if (getYMultiplier(this->direction)) this->y = getRoundedPosition(this->y, this->direction);
    this->manageTurnRequest(change);
    this->place();
    return;
  }
  
  this->x = this->x + change * dx;
  this->y = this->y + change * dy;

  if (dx > 0 && this->x > 21*MOVE_UNIT) this->x = 0;
  if (dx < 0  && this->x < 0) this->x = 21*MOVE_UNIT;

  this->place();
}

void Pacman::adjustYaw(float time, float destAngle) {
  //std::cout<<this->yaw<<" to "<<destAngle<<"\n";
  if (absf(this->yaw-(destAngle+360.0)) < absf(this->yaw-(destAngle)))
    destAngle += 360.0;
  if (absf(this->yaw-(destAngle-360.0)) < absf(this->yaw-(destAngle)))
    destAngle -= 360.0;
  float change = ROTATION_SPEED*time*sign(destAngle-this->yaw);
  if ((change > 0 && this->yaw + change >= destAngle) ||
      (change < 0 && this->yaw + change <= destAngle)) {
    this->yaw = destAngle;
    return;
  }
  this->yaw += change;
  if (this->yaw >= 360) this->yaw -= 360;
  else if (this->yaw <= -360) this->yaw += 360;
}

void Pacman::place() {
  this->model->meshes[0].transform = glm::rotate(glm::mat4(1.0f), glm::radians(-this->mouthAngle), glm::vec3(0,0,1));
  this->model->meshes[1].transform = glm::rotate(glm::mat4(1.0f), glm::radians(this->mouthAngle), glm::vec3(0,0,1));
  //this->model->meshes[2].transform = glm::translate(glm::mat4(1.0f), glm::vec3(0,8,0));
  this->pos = glm::vec3(-39.87 + this->x*WORLD_UNIT, 1, -39.87 + this->y*WORLD_UNIT);
  glm::mat4 translated = glm::translate(glm::mat4(1.0f), this->pos);
  this->model->transform = glm::rotate(translated, glm::radians(this->yaw), glm::vec3(0,1,0));
}

void Pacman::draw(Shader* shader) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  this->model->draw(shader);
}

void Pacman::rotate(int argDirection, bool relativeToPacman) {
  if (!relativeToPacman) {
    this->newDirection = argDirection;
    return;
  }
  if (argDirection == DIRECTION_RIGHT){
    this->newDirection = rotateRight(this->direction);
  }
  else if (argDirection == DIRECTION_LEFT){
    this->newDirection = rotateLeft(this->direction);
  }
  else if (argDirection == DIRECTION_DOWN){
    this->newDirection = -1 * this->direction;
  }
}

void Pacman::cancelRotate(int argDirection) {
  //if (this->newDirection == argDirection)
  //this->newDirection = this->direction;
}

int Pacman::getDirection(){
  return direction;
}
int Pacman::getX(){
  return x/MOVE_UNIT;
}
int Pacman::getY(){
  return y/MOVE_UNIT;
}

