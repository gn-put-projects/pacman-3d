#include "Game.h"

const std::unordered_map<int, int> pacmanKeybinds = {
  {GLFW_KEY_W, DIRECTION_UP},
  {GLFW_KEY_S, DIRECTION_DOWN},
  {GLFW_KEY_A, DIRECTION_LEFT},
  {GLFW_KEY_D, DIRECTION_RIGHT}
};

const std::unordered_map<int, int> cameraKeybinds = {
  {GLFW_KEY_F, CAMERA_LEFT},
  {GLFW_KEY_T, CAMERA_FORWARD},
  {GLFW_KEY_G, CAMERA_BACKWARD},
  {GLFW_KEY_H, CAMERA_RIGHT},
  {GLFW_KEY_R, CAMERA_UP},
  {GLFW_KEY_Y, CAMERA_DOWN},
  {GLFW_KEY_UP, CAMERA_PITCH_UP},
  {GLFW_KEY_DOWN, CAMERA_PITCH_DOWN},
  {GLFW_KEY_LEFT, CAMERA_YAW_LEFT},
  {GLFW_KEY_RIGHT, CAMERA_YAW_RIGHT},
  {GLFW_KEY_C, MODE_CHANGE}
};

const std::unordered_map<int, int> gameKeybinds = {
};

Game::Game(GLFWwindow* window) {
  this->window = window;
  this->board = new Board();
  this->pacman = new Pacman(this);
  this->foods.clear();
  for (int y=0; y<19; ++y){
    for (int x=0; x<19; ++x){
      if (!this->wall(x+1,y+1) && !(x+1>4 && y+1>5 && x+1<16 && y+1<13))
	this->foods.push_back({150+x*100, 150+y*100});
    }
  }
  this->food = new Food(this, this->foods);
  this->success = true;
  if (!this->pacman->success) {
    this->success = false;
    this->failureMsg = "Game: " + this->pacman->failureMsg;
    return;
  }
  this->camera = new Camera(this);
  this->blinky = new Ghost(this, "blinky");
  this->pinky = new Ghost(this, "pinky");
  this->inky = new Ghost(this, "inky");
  this->clyde = new Ghost(this, "clyde");
}

Game::~Game() {
  if (this->board)
    delete this->board;
  if (this->pacman)
    delete this->pacman;
  if (this->camera)
    delete this->camera;
  if (this->blinky)
    delete this->blinky;
  if (this->pinky)
    delete this->pinky;
  if (this->inky)
    delete this->inky;
  if (this->clyde)
    delete this->clyde;
  if (this->food)
    delete this->food;
}

void Game::drawTerm() {
  this->board->drawTerm();
}

void Game::keyDown(int key) {
  if (pacmanKeybinds.contains(key)) {
    this->pacman->rotate(pacmanKeybinds.at(key),
			 this->camera->mode==CAM_FOLLOW);
  }
  if (cameraKeybinds.contains(key)) {
    this->camera->move(cameraKeybinds.at(key));
  }
  if (gameKeybinds.contains(key)) {
  }
}

void Game::keyUp(int key) {
  if (pacmanKeybinds.contains(key)) {
    this->pacman->cancelRotate(pacmanKeybinds.at(key));
  }
  if (cameraKeybinds.contains(key)) {
    this->camera->stopMove(cameraKeybinds.at(key));
  }
}

bool Game::wall(int x, int y) {
  return this->board->wall(x,y);
}

void Game::update(double time) {
  this->pacman->update(time);
  this->food->update(time);
  this->camera->update(time);
  
  this->blinky->update(time);
  this->pinky->update(time);
  this->inky->update(time);
  this->clyde->update(time);
}

void Game::render(Shader* shader) {
  glm::vec3 camPos = this->camera->pos;
  glm::vec3 light = glm::vec3(2,10,0);
  glm::vec3 lightColor = glm::vec3(1.0,1.0,1.0);

  glm::vec3 objectColor = glm::vec3(1.0,0.5,0.5);
  
  glm::mat4 P = glm::perspective
    (glm::radians(50.0f), 1.0f, 1.0f, 150.0f);
  glm::mat4 V = glm::lookAt
    (this->camera->pos,
     this->camera->lookAt,
     glm::vec3(0.0f, 1.0f, 0.0f));
  
  glm::mat4 M = glm::mat4(1.0f);
    
  shader->use();

  glUniformMatrix4fv(shader->uni("P"), 1, false, glm::value_ptr(P));
  glUniformMatrix4fv(shader->uni("V"), 1, false, glm::value_ptr(V));
  glUniformMatrix4fv(shader->uni("M"), 1, false, glm::value_ptr(M));

  glUniformMatrix3fv(shader->uni("lightColor"), 1, false, glm::value_ptr(lightColor));
  glUniformMatrix3fv(shader->uni("objectColor"), 1, false, glm::value_ptr(objectColor));
  glUniformMatrix3fv(shader->uni("lightPos"), 1, false, glm::value_ptr(light));
  glUniformMatrix3fv(shader->uni("viewPos"), 1, false, glm::value_ptr(camPos));
  
  this->board->draw(shader);
  
  this->pacman->draw(shader);
  this->blinky->draw(shader);
  this->pinky->draw(shader);
  this->inky->draw(shader);
  this->clyde->draw(shader);

  this->food->drawArray(shader);
  
  //for (uint i=0; i<this->foods.size(); ++i){
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  //}
}

void Game::eat(int x, int y) {
  if (this->food->eatFrom(x, y)) {
    this->blinky->setFrightenedMode();
    this->pinky->setFrightenedMode();
    this->inky->setFrightenedMode();
    this->clyde->setFrightenedMode();
  }
}

Board* Game::getBoard(){
  return this->board;
}

Pacman* Game::getPacman(){
  return this->pacman;
}

Ghost* Game::getBlinky(){
  return this->blinky;
}
