#ifndef GHOST_H
#define GHOST_H

class Ghost;
class Blinky;
class Pinky;
class Inky;
class Clyde;

#include <iostream>
#include "Game.h"
#include "Model.h"
#include "Shader.h"

#define COLLISION_DISTANCE 30.0

class Point{
    public:
    int x;
    int y;
    Point(){}
    Point(int x, int y){
        this->x = x;
        this->y = y;
    }
};

typedef enum { Normal, Scatter, Frightened } Behavior;

typedef enum { Sleeping, Exiting, Chasing } Goal;

typedef enum {None = -1, Up = 0, Right = 1, Down = 2, Left = 3} Direction;

class Ghost{
public:
  std::string name;
  Point positionOnBoard;
  Point scatterCorner;
  Behavior behavior; // 0 -> normal 1 -> scatter 2 -> frightened 
  Point target;
  
  Goal goal;// 0 -> sleeping 1 -> leaving house 2 -> outside house
  Direction blocked; // which way he can't go 
  Game* game;
  
  double speed; //units per second
  Model* vulnerableModel;
  Model* normalModel;
  Model* model;
  
  Point position;
  double normalTime;
  double scatterTime;
  double frightenedTime;
  double leavingTime;
  
  Ghost(Game*, string);
  void wakeUpGhost();
  void display();
  void setChaseMode();
  void setScatterMode();
  void setFrightenedMode();
  void update(double);
  ~Ghost();
  void place();
  void draw(Shader* shader);
  
private:
  void init(string, bool);
  int choosePath();
  void makeDecision();
  void makeDecisionPinky();
  void makeDecisionInky();
  void makeDecisionClyde();
  void moveOnBoard();
  bool collidesWithPacman();
};

#endif
