#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <vector>
#include <unordered_map>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

using std::vector;
typedef unsigned int uint;

class Mesh;

#include "Shader.h"

struct Vertex {
  glm::vec3 Position;
  glm::vec3 Normal;
  glm::vec2 TexCoords;
  glm::vec3 Tangent;
  glm::vec3 Bitangent;
};
typedef struct Vertex Vertex;


struct Texture {
  unsigned int id;
  string type;
  string path;
};
typedef struct Texture Texture;

class Mesh {
private:
  uint VAO;
  uint VBO;
  uint EBO;
public:
  glm::mat4 transform;
  vector<Vertex> vertices;
  vector<uint> indices;
  vector<Texture> textures;
  
  void draw(Shader*, glm::mat4&);
  void drawArray(Shader*, std::unordered_map<int, glm::mat4>*);
  Mesh(vector<Vertex>, vector<uint>, vector<Texture>);
  ~Mesh();
};

#endif
