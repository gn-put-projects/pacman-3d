#include "Ghost.h"
#include <cmath>

std::unordered_map<string, Point> startingPositions = {
  {"blinky",{11, 9}},
  {"pinky", {9, 9}},
  {"inky", {12, 9}},
  {"clyde", {8, 9}}
};

std::unordered_map<string, Point> scatterCorners = {
  {"blinky",{BOARD_WIDTH, 0}},
  {"pinky", {0, 0}},
  {"inky", {BOARD_WIDTH, BOARD_HEIGHT}},
  {"clyde", {0, BOARD_HEIGHT}}
};

std::unordered_map<string, double> leavingTimes = {
  {"blinky", 1.0},
  {"pinky", 2.0},
  {"inky", 3.0},
  {"clyde", 4.0}
};

int roundPos (float x, int dir) {
  //dir specifies whether the value is rising (1) or falling (-1)
  if (dir<0)
    return int(x/(MOVE_UNIT/2) + 1)*MOVE_UNIT/2;
  return int(x/(MOVE_UNIT/2))*MOVE_UNIT/2;
}

float absfl(float x) {
  return (x<0)? -x : x;
}
int getRoundedPos (float x, int dir) {
  if (dir<0)
    return roundPos(x,dir)-50;
  return roundPos(x,dir);
}

// based on blocked
const std::unordered_map<int, float> notfacingAngle = {
  {Down, 90.0f},
  {Up, -90.0f},
  {Right, 180.0f},
  {Left, 0.0f},
  {None, -90.0f}
};


float distance(int px, int py, int kx, int ky) {
  int x = abs(px - kx);
  int y = abs(py - ky);
  return sqrt(x * x + y * y);
}

Ghost::Ghost(Game* game, string name) {
  this->game = game;
  this->vulnerableModel = new Model("models/ghost.obj", false);

  this->init(name, true);
}

Ghost::~Ghost() {
  delete this->vulnerableModel;
  delete this->normalModel;
}

void Ghost::display() {
  std::cout << "Name: " << name << std::endl;
  std::cout << "PositionOnBoard X: " << positionOnBoard.x << std::endl;
  std::cout << "PositionOnBoard Y: " << positionOnBoard.y << std::endl;
  std::cout << "Scatter corner X: " << scatterCorner.x << std::endl;
  std::cout << "Scatter corner Y: " << scatterCorner.y << std::endl;
  std::cout << "State: " << behavior << std::endl;
  std::cout << "Moving to X: " << target.x << std::endl;
  std::cout << "Moving to Y: " << target.y << std::endl;
  std::cout << "Blocked path to go: " << blocked << std::endl;
  std::cout << "Ghost state: " << goal << std::endl;
  std::cout << std::endl;
}

void Ghost::update(double time){
  Point posBefore;
  posBefore.x = positionOnBoard.x;
  posBefore.y = positionOnBoard.y;
  double change = this->speed * time;
    
  if (change >= MOVE_UNIT/4) {
    printf("too laggy!\n");
    return;
  }
    
  if(blocked == Up) {
    if(position.y + change < positionOnBoard.y*MOVE_UNIT + MOVE_UNIT*0.5) this->position.y += change*1.45;
    else this->position.y = positionOnBoard.y*MOVE_UNIT + MOVE_UNIT*0.5;
  }
  if(blocked == Right) {
    if(position.x - change > positionOnBoard.x*MOVE_UNIT + MOVE_UNIT*0.5) this->position.x -= change;
    else this->position.x = positionOnBoard.x*MOVE_UNIT + MOVE_UNIT*0.5;
  }
  if(blocked == Down){
    if(position.y - change > positionOnBoard.y*MOVE_UNIT + MOVE_UNIT*0.5) this->position.y -= change;
    else this->position.y = positionOnBoard.y*MOVE_UNIT + MOVE_UNIT*0.5;
  } 
  if(blocked == Left){
    if(position.x + change < positionOnBoard.x*MOVE_UNIT + MOVE_UNIT*0.5) this->position.x += change*1.45;
    else this->position.x = positionOnBoard.x*MOVE_UNIT + MOVE_UNIT*0.5;
  }
    
  if(position.x - positionOnBoard.x*MOVE_UNIT  - MOVE_UNIT*0.5 == 0 &&
     position.y - positionOnBoard.y*MOVE_UNIT - MOVE_UNIT*0.5 == 0 ) moveOnBoard();

  this->place();
  
  if(goal == Sleeping){
   if(this->leavingTime <= 0) wakeUpGhost();
   else this->leavingTime -= time;
   return;
  }

  if(goal == Chasing){
    if(behavior == Normal){
      if(this->normalTime < 20 ) this->normalTime += time;
      else {
        this -> normalTime = 0;
        setScatterMode();
      }
      return;
    }

    if(behavior == Scatter){
      if(this-> scatterTime < 7 ) this->scatterTime +=time;
      else{
        this->scatterTime = 0;
        setChaseMode();
      }
      return;
    }

    if(behavior == Frightened){
      //normal
      if(this-> frightenedTime < 10) this->frightenedTime +=time;
      else{
        this->frightenedTime = 0;
        setChaseMode();
      }

      //blinking at the end of frightenedMode
      if(this->frightenedTime > 8.5) this->model = normalModel;
      if(this->frightenedTime > 9 )this->model = vulnerableModel;
      if(this->frightenedTime > 9.25) this->model = normalModel;
      if(this->frightenedTime > 9.5 )this->model = vulnerableModel;
      if(this->frightenedTime > 9.65) this->model = normalModel;
      if(this->frightenedTime > 9.85 )this->model = vulnerableModel;
      this->place();
      
      if (this->collidesWithPacman()){
        this->init(this->name, false);
        this->leavingTime = 10;
	this->place();
	return;
      }
    }
  }
  
}

bool Ghost::collidesWithPacman() {
  Point pacmanPos(this->game->getPacman()->x,this->game->getPacman()->y);
  return (absfl(pacmanPos.x-position.x) <= COLLISION_DISTANCE && absfl(pacmanPos.y-position.y) <= COLLISION_DISTANCE);
}

void Ghost::moveOnBoard() {
  if (goal == Sleeping) {
    return;
  }
    
  int pathsToGo = 0;
  Point change(0,0);
  Direction estimatedBlockedWay = None;
  Board* board;
  board = game->getBoard();

  // try go up
  if (positionOnBoard.y - 1 >= 0 && board->getBoardValue(positionOnBoard.x, positionOnBoard.y - 1) && blocked != Up) {
    pathsToGo++;
    estimatedBlockedWay = Down;
    change.y = -1;
  }

  // try go right
  if (positionOnBoard.x + 1 <= 20 && board->getBoardValue(positionOnBoard.x + 1, positionOnBoard.y) && blocked != Right) {
    pathsToGo++;
    estimatedBlockedWay = Left;
    change.x = 1;
  }

  // try go down
  if (positionOnBoard.y + 1 <= 20 && board->getBoardValue(positionOnBoard.x, positionOnBoard.y + 1) && blocked != Down) {
    // if you want to enter the house deny it
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (!(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y + 1)) {
      pathsToGo++;
      estimatedBlockedWay = Up;
      change.y = 1;
    }
  }

  // try go left
  if (positionOnBoard.x - 1 >= 0 && board->getBoardValue(positionOnBoard.x - 1, positionOnBoard.y) && blocked != Left) {
    pathsToGo++;
    estimatedBlockedWay = Right;
    change.x = -1;
  }
 
  if (pathsToGo == 1) {
    positionOnBoard.x += change.x;
    positionOnBoard.y += change.y;
    blocked = estimatedBlockedWay;
  }

  if (pathsToGo > 1) {
    makeDecision();

    int newPath = choosePath();
    change.x = 0;
    change.y = 0;

    switch (newPath) {
    case 0: {
      change.y = -1;
      blocked = Down;
      break;
    }
    case 1: {
      change.x = 1;
      blocked = Left;
      break;
    }
    case 2: {
      change.y = 1;
      blocked = Up;
      break;
    }
    case 3: {
      change.x = -1;
      blocked = Right;
      break;
    }
    default: {
      std::cout << name << " by using choosePath() didn't choose any path" << std::endl;
      exit(1);
      break;
    }
    }

    positionOnBoard.x += change.x;
    positionOnBoard.y += change.y;

  }

  // teleport
  if (pathsToGo < 1) {

    if (this->positionOnBoard.x == 0 && this->positionOnBoard.y == 9)
      this->positionOnBoard.x = 21;
    else
      if (this->positionOnBoard.x == 20 && this->positionOnBoard.y == 9)
	      this->positionOnBoard.x = 0;
            
    this->position.x = this->positionOnBoard.x * MOVE_UNIT + MOVE_UNIT*0.5;
    this->position.y = this->positionOnBoard.y * MOVE_UNIT + MOVE_UNIT*0.5;
    this->model->translate = glm::vec3(-39.87 + this->position.x*WORLD_UNIT, 1,
				       -39.87 + this->position.y*WORLD_UNIT);

  }
}

void Ghost::wakeUpGhost() {
  goal = Exiting;
  moveOnBoard();
}

void Ghost::setChaseMode() {
  behavior = Normal;
  this->model = normalModel;
  this->speed = 2*MOVE_UNIT;
}

void Ghost::setScatterMode() {
  if (goal == Chasing) {
    target.x = scatterCorner.x;
    target.y = scatterCorner.y;
    behavior = Scatter;
  }
}

void Ghost::setFrightenedMode() {
  if (goal != Chasing) return;
  behavior = Frightened;
  makeDecision();
  this->model = this->vulnerableModel;
  this->speed = MOVE_UNIT;
}

int Ghost::choosePath() {
  Direction dir = None;
  float CurrentMinDistance = 1000000;
  float pom = distance(target.x, target.y, positionOnBoard.x, positionOnBoard.y - 1);
  Board* board;
  board = game->getBoard();

  if (pom < CurrentMinDistance && board->getBoardValue(positionOnBoard.x, positionOnBoard.y - 1) && blocked != Up) {
    CurrentMinDistance = pom;
    dir = Up;
  }
  
  pom = distance(target.x, target.y, positionOnBoard.x + 1, positionOnBoard.y);

  if (pom < CurrentMinDistance && board->getBoardValue(positionOnBoard.x + 1, positionOnBoard.y) && blocked != Right) {
    CurrentMinDistance = pom;
    dir = Right;
  }
  
  pom = distance(target.x, target.y, positionOnBoard.x, positionOnBoard.y + 1);

  if (pom < CurrentMinDistance && board->getBoardValue(positionOnBoard.x, positionOnBoard.y + 1) && blocked != Down) {
    // deny attempt entering ghost House
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (!(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y)) {
      CurrentMinDistance = pom;
      dir = Down;
    }
  }
  
  pom = distance(target.x, target.y, positionOnBoard.x - 1, positionOnBoard.y);

  if (pom < CurrentMinDistance && board->getBoardValue(positionOnBoard.x - 1, positionOnBoard.y) && blocked != Left) {
    CurrentMinDistance = pom;
    dir = Left;
  }
  
  return dir;
}

void Ghost::makeDecision() {
  if (this->name == "pinky"){
    this->makeDecisionPinky();
    return;
  }
  if (this->name == "clyde"){
    this->makeDecisionClyde();
    return;
  }
  if (this->name == "inky"){
    this->makeDecisionInky();
    return;
  }
  std::cout<<"Decision: "<<this->name<<"\n";
  Point pacmanPos(this->game->getPacman()->getX(),this->game->getPacman()->getY());
  int boardX = this->game->getBoard()->getWidth();
  int boardY = this->game->getBoard()->getHeight();
  if (behavior == Normal) {
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (goal == Exiting && !(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y))
      return;
    goal = Chasing;
    target.x = pacmanPos.x;
    target.y = pacmanPos.y;
  }
    
  if (behavior == Frightened) {
    if (pacmanPos.x - positionOnBoard.x > 0)
      target.x = 0;
    if (pacmanPos.x - positionOnBoard.x < 0)
      target.x = boardX;
    if (pacmanPos.x - positionOnBoard.x == 0)
      target.x = positionOnBoard.x;
    
    if (pacmanPos.y - positionOnBoard.y > 0)
      target.y = 0;
    if (pacmanPos.y - positionOnBoard.y < 0)
      target.y = boardY;
    if (pacmanPos.y - positionOnBoard.y == 0)
      target.y = positionOnBoard.y;
  }
}

void Ghost::place() {
  this->model->transform = glm::rotate(glm::mat4(1.0f), glm::radians(notfacingAngle.at(this->blocked)), glm::vec3(0,1,0));
  this->model->translate = glm::vec3(-39.87 + this->position.x*WORLD_UNIT, 1, -39.87 + this->position.y*WORLD_UNIT);
}

void Ghost::draw(Shader* shader) {
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  this->model->draw(shader);
}

void Ghost::makeDecisionPinky() {
  int pacmanFacing = game->getPacman()->getDirection();
  Point pacmanPos(this->game->getPacman()->getX(),this->game->getPacman()->getY());
  if (behavior == Normal) {
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (goal == Exiting && !(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y))
      return;
    goal = Chasing;
    target.x = pacmanPos.x;
    target.y = pacmanPos.y;
    
    if (pacmanFacing == Up)
      target.y -= 4;
    if (pacmanFacing == Right)
      target.x += 4;
    if (pacmanFacing == Down)
      target.y += 4;
    if (pacmanFacing == Left)
      target.x -= 4;
  }
    
    if (behavior == Frightened) {
      int boardX = this->game->getBoard()->getWidth();
      int boardY = this->game->getBoard()->getHeight();
      if (pacmanPos.x - positionOnBoard.x > 0)
	      target.x = 0;
      if (pacmanPos.x - positionOnBoard.x < 0)
	      target.x = boardX;
      if (pacmanPos.x - positionOnBoard.x == 0)
	      target.x = positionOnBoard.x;
      
      if (pacmanPos.y - positionOnBoard.y > 0)
	      target.y = 0;
      if (pacmanPos.y - positionOnBoard.y < 0)
	      target.y = boardY;
      if (pacmanPos.y - positionOnBoard.y == 0)
	      target.y = positionOnBoard.y;
    }
}

void Ghost::makeDecisionInky() {
  int pacmanFacing = game->getPacman()->getDirection();
  Point pacmanPos(this->game->getPacman()->getX(),this->game->getPacman()->getY());
  if (behavior == Normal) {
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (goal == Exiting && !(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y))
      return;

    goal = Chasing;
    Point blinkyPos(this->game->getBlinky()->positionOnBoard.x,this->game->getBlinky()->positionOnBoard.y);
    int dystanceX = pacmanPos.x - blinkyPos.x;
    int dystanceY = pacmanPos.y - blinkyPos.y;

    if (pacmanFacing == Up)
      dystanceY -= 4;
    if (pacmanFacing == Right)
      dystanceX += 4;
    if (pacmanFacing == Down)
      dystanceY += 4;
    if (pacmanFacing == Left)
      dystanceX -= 4;

    target.x = pacmanPos.x + dystanceX;
    target.y = pacmanPos.y + dystanceY;
  }
    
  if (behavior == Frightened) {
    int boardX = this->game->getBoard()->getWidth();
    int boardY = this->game->getBoard()->getHeight();
    if (pacmanPos.x - positionOnBoard.x > 0)
      target.x = 0;
    if (pacmanPos.x - positionOnBoard.x < 0)
      target.x = boardX;
    if (pacmanPos.x - positionOnBoard.x == 0)
      target.x = positionOnBoard.x;

    if (pacmanPos.y - positionOnBoard.y > 0)
      target.y = 0;
    if (pacmanPos.y - positionOnBoard.y < 0)
      target.y = boardY;
    if (pacmanPos.y - positionOnBoard.y == 0)
      target.y = positionOnBoard.y;
  }
}

void Ghost::init(string name, bool loadModel) {
  this->name = name;
  this->behavior = Normal;
  this->target = Point(this->game->getBoard()->getGHEntranceX(),
		       this->game->getBoard()->getGHEntranceY());
  this->goal = Sleeping;
  this->blocked = Down;
  this->positionOnBoard = startingPositions.at(this->name);
  this->scatterCorner = scatterCorners.at(this->name);
  this->position = Point(this->positionOnBoard.x * MOVE_UNIT + 50 , this->positionOnBoard.y * MOVE_UNIT + 50);
  if (loadModel)
    this->normalModel = new Model("models/ghost.obj", false, name+".png");
  this->model = this->normalModel;
  this->leavingTime = leavingTimes.at(this->name);
  this->speed = 2*MOVE_UNIT;
  this->normalTime = 0;
  this->scatterTime = 0;
  this->frightenedTime = 0;
}

void Ghost::makeDecisionClyde() {
  Point pacmanPos(this->game->getPacman()->getX(),this->game->getPacman()->getY());
  if (behavior == Normal) {
    int GHEntranceX = this->game->getBoard()->getGHEntranceX();
    int GHEntranceY = this->game->getBoard()->getGHEntranceY();
    if (goal == Exiting && !(GHEntranceX == positionOnBoard.x && GHEntranceY == positionOnBoard.y))
      return;
    goal = Chasing;

    bool tooClose = false;
    if (abs(pacmanPos.x - positionOnBoard.x) < 8 && abs(pacmanPos.y - positionOnBoard.y) < 8)
      tooClose = true;

    if (tooClose) {
      target.x = scatterCorner.x;
      target.y = scatterCorner.y;
    } else {
      target.x = pacmanPos.x;
      target.y = pacmanPos.y;
    }
  }

  if (behavior == Frightened) {
    int boardX = this->game->getBoard()->getWidth();
    int boardY = this->game->getBoard()->getHeight();
    if (pacmanPos.x - positionOnBoard.x > 0)
      target.x = 0;
    if (pacmanPos.x - positionOnBoard.x < 0)
      target.x = boardX;
    if (pacmanPos.x - positionOnBoard.x == 0)
      target.x = positionOnBoard.x;
        
    if (pacmanPos.y - positionOnBoard.y > 0)
      target.y = 0;
    if (pacmanPos.y - positionOnBoard.y < 0)
      target.y = boardY;
    if (pacmanPos.y - positionOnBoard.y == 0)
      target.y = positionOnBoard.y;
  }
}
