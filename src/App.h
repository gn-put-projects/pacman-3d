#ifndef APP_H
#define APP_H

#include <cstdio>
#include <iostream>
#include <string>

#define GLM_FORCE_RADIANS

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>

#include "Game.h"
#include "Shader.h"

using std::string;

#define WINDOW_SIZE_X 500
#define WINDOW_SIZE_Y 500

class App {
 private:
  GLFWwindow* window;
  bool success; // value set to false in order to exit App.loop()
  string failureMsg; // error information
  Game* game;
  Shader* shader;
 public:
  void loop();
  bool checkAbort();
  void abort(string);
  void draw();
  void keyCallback(GLFWwindow*, int, int, int, int);
  App();
  ~App();
};

#endif
