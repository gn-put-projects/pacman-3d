#ifndef CAMERA_H
#define CAMERA_H

#include <iostream>
#include <glm/glm.hpp>

#define CAMERA_SPEED_X 30.0
#define CAMERA_SPEED_Y 30.0
#define CAMERA_SPEED_Z 30.0

#define CAMERA_PITCH_SPEED 120.0
#define CAMERA_YAW_SPEED 120.0

#define CAMERA_LEFT 0
#define CAMERA_FORWARD 1
#define CAMERA_BACKWARD 2
#define CAMERA_RIGHT 3
#define CAMERA_UP 4
#define CAMERA_DOWN 5
#define CAMERA_PITCH_DOWN 6
#define CAMERA_PITCH_UP 7
#define CAMERA_YAW_LEFT 8
#define CAMERA_YAW_RIGHT 9
#define MODE_CHANGE 10

// MODES
#define CAM_FREE -1
#define CAM_FOLLOW 1

#define FOLLOW_DISTANCE 14
#define FOLLOW_DISTANCE_SPEED 20.0
#define FOLLOW_HEIGHT 9
#define FOLLOW_HEIGHT_SPEED 20.0

class Camera;

#include "Game.h"

class Camera {
private:
  Game* game;
  double yaw;
  double pitch;
  glm::vec3 changeSpeed;
  float followDistance;
  float followDistanceSpeed;
  float followHeight;
  float followHeightSpeed;
public:
  glm::vec3 pos;
  glm::vec3 prevPos;
  glm::vec3 lookAt;
  glm::vec3 dir;
  int mode;
  glm::vec3 speed;
  glm::vec3 rotspeed;
  void follow(glm::vec3, float);
  void move(int);
  void update(double);
  void stopMove(int);
  Camera(Game*);
  ~Camera();
};

#endif
