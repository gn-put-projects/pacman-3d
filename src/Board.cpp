#include "Board.h"

const int default_board[] = {
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
  0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,
  0,1,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,1,0,
  0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
  0,1,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,1,0,
  0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,
  0,0,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,0,0,
  0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,
  0,0,0,0,1,0,1,0,0,0,1,0,0,0,1,0,1,0,0,0,0,
  1,1,1,1,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,1,1,
  0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,
  0,0,0,0,1,0,1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,
  0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,
  0,1,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,
  0,1,0,0,1,0,0,0,0,1,0,1,0,0,0,0,1,0,0,1,0,
  0,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,1,1,0,
  0,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,1,0,1,0,0,
  0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,
  0,1,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,
  0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,
  0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

Board::Board() {
  this->rows = BOARD_HEIGHT;
  this->columns = BOARD_WIDTH;
  this->GHEntranceX = 10;
  this->GHEntranceY = 7;
  this->model = new Model("models/board.obj", false);
  this->model->scale = glm::vec3(4,4,4);
  this->model->translate = glm::vec3(0,-1,0);
  for (int y=0; y<21;++y) {
    for (int x=0;x<21;++x) {
      this->b[x][y] = default_board[x+y*21];
    }
  }
}

void Board::draw(Shader* shader) {
  this->model->draw(shader);
}

void Board::drawTerm() {
  for (int y=0; y<21;++y) {
    for (int x=0;x<21;++x) {
      std::cout<<((this->b[x][y])? " " : "#");
    }
    std::cout<<"\n";
  }
}

Board::~Board() {
  if (this->model)
    delete this->model;
}

bool Board::wall(int x, int y) {
  return this->b[x][y] == 0;
}

int Board::getBoardValue(int x, int y){
  return b[x][y];
}

int Board::getWidth(){
  return columns;
}
int Board::getHeight(){
  return rows;
}
int Board::getGHEntranceX(){
  return GHEntranceX;
}
int Board::getGHEntranceY(){
  return GHEntranceY;
}
