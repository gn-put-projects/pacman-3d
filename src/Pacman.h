#ifndef PACMAN_H
#define PACMAN_H

#include <cstdio>
#include <iostream>
#include <string>
#include <unordered_map>
//#include <GLFW/glfw3.h>

class Pacman;

#include "Game.h"
#include "Model.h"

using std::string;

#define PACMAN_STARTPOS_X 10
#define PACMAN_STARTPOS_Y 15
#define DIRECTION_STOP 0
#define DIRECTION_UP -1
#define DIRECTION_DOWN 1
#define DIRECTION_LEFT -2
#define DIRECTION_RIGHT 2

#define MOVE_UNIT 100.0

#define WORLD_UNIT 0.038

#define MOUTH_SPEED 300.0f
#define MAX_MOUTH_OPEN_ANGLE 90.0f
#define MOUTH_OPEN 1
#define MOUTH_CLOSE -1

#define ROTATION_SPEED 350.0


class Pacman {
private:
  int direction;
  int newDirection;
  double speed; //units per second
  Model* model;
  Game* game;
  void place();
  int nextX(double, int, bool);
  int nextY(double, int, bool);
  void adjustYaw(float, float);
  void manageTurnRequest(double);
  int mouthAction;
  float mouthAngle;
  float mouthSpeed;
  void moveMouth(double);
public:
  double x;
  double y;
  glm::vec3 pos;
  bool success;
  string failureMsg;
  float yaw;
  glm::vec3 get3rdPersonPos();
  void update(double time);
  void draw(Shader* shader);
  void rotate(int, bool);
  void cancelRotate(int);
  Pacman(Game*);
  ~Pacman();
  int getDirection();
  int getX();
  int getY();
};

#endif
