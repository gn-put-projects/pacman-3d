#include "Shader.h"

bool checkCompileErrors(unsigned int shader, string name) {
  int success;
  char infoLog[1024];
  string error;
  if (name == "program") {
    glGetProgramiv(shader, GL_LINK_STATUS, &success);
    name = "shader " + name;
    error = "Error compiling ";
  }
  else {
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    name = name + " shader";
    error = "Error linking ";
  }
  
  if (!success) {
    glGetShaderInfoLog(shader, 1024, NULL, infoLog);
    std::cout<<error<<" "<<name<<":\n"<<infoLog<<"\n------------------\n";
    return true;
  }
  return false;
}

Shader::Shader(cstr vsPath, cstr fsPath) {
  this->error = false;
  std::ifstream vsFile, fsFile;

  string vsCodeStr, fsCodeStr;
  
  // Enable exceptions
  vsFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  fsFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
  
  try {
    vsFile.open(vsPath);
    fsFile.open(fsPath);
    std::stringstream vsStream, fsStream;
    
    vsStream << vsFile.rdbuf();
    fsStream << fsFile.rdbuf();
    
    vsFile.close();
    fsFile.close();
    
    vsCodeStr = vsStream.str();
    fsCodeStr = fsStream.str();
  }
  catch (std::ifstream::failure& e) {
    std::cout<<"Error reading shader files: "<<e.what()<<"\n";
    this->error = true;
  }
  cstr vsCode = vsCodeStr.c_str();
  cstr fsCode = fsCodeStr.c_str();

  uint vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, &vsCode, NULL);
  glCompileShader(vertex);
  this->error = this->error || checkCompileErrors(vertex, "vertex");
  
  uint fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, &fsCode, NULL);
  glCompileShader(fragment);
  this->error = this->error || checkCompileErrors(fragment, "fragment");

  this->id = glCreateProgram();
  glAttachShader(this->id, vertex);
  glAttachShader(this->id, fragment);
  glLinkProgram(this->id);
  this->error = this->error || checkCompileErrors(this->id, "program");
  
  glDeleteShader(vertex);
  glDeleteShader(fragment);
}

void Shader::use() { 
  glUseProgram(this->id); 
}

GLuint Shader::uni(const string& varname) {
  return glGetUniformLocation(this->id, varname.c_str());
}

GLuint Shader::atr(const string& varname) {
  return glGetAttribLocation(this->id, varname.c_str());
}

void Shader::setValue(const string& name, int value) {
  glUniform1i(glGetUniformLocation(this->id, name.c_str()), value); 
}

Shader::~Shader() {

}
