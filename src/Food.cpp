#include "Food.h"

std::set<int> superFoods = {
  {1+2*POS_ENCODE},
  {1+18*POS_ENCODE},
  {19+2*POS_ENCODE},
  {19+18*POS_ENCODE}
};

float fmod(float l, float r) {
  return l - int(l/r)*r;
}

Food::Food(Game* game, vector<glm::vec2>& positions) {
  this->game = game;
  this->height = 0;
  this->model = new Model("models/food.obj", false);
  this->success = this->model->success;
  this->failureMsg = "Food: " + this->model->failureMsg;
  this->timePassed = 0;
  this->Ms.clear();
  for (uint i=0; i<positions.size();++i) {
    this->Ms.insert({int(positions[i].x/MOVE_UNIT)+int((positions[i].y/MOVE_UNIT))*POS_ENCODE, glm::mat4(1.0f)});
  }
}

bool Food::eatFrom(int X, int Y) {
  // Returns true if superfood was eaten. False otherwise
  if (!this->Ms.contains(X+Y*POS_ENCODE))
    return false;
  this->Ms.erase(X+Y*POS_ENCODE);
  return superFoods.contains(X+Y*POS_ENCODE);
}
Food::~Food() {
  delete this->model;
}

void Food::update(float time) {
  this->timePassed+=time;
  if (timePassed > 2*PI) timePassed-=2*PI;
  for (uint y=0; y<21;++y) {
    for (uint x=0; x<21;++x) {
      if (!Ms.contains(x+y*POS_ENCODE)) continue;
      this->height = sin((this->timePassed+(float(x+y)/WAVE_LENGTH))*LEVITATE_SPEED)*LEVITATE_RANGE;
      this->place(x*MOVE_UNIT+MOVE_UNIT/2, y*MOVE_UNIT+MOVE_UNIT/2);
      glm::mat4 M = glm::mat4(1.0f);
      M = glm::translate(M, this->model->translate);
      if (superFoods.contains(x+y*POS_ENCODE)) {
	M = glm::scale(M, this->model->scale*SUPERFOOD_SCALE);
      }
      else M = glm::scale(M, this->model->scale);
      this->Ms.at(x+y*POS_ENCODE) = M;
    }
  }
}

void Food::place(float x, float y) {
  this->model->scale = glm::vec3(0.3,0.3,0.3);
  this->model->translate = glm::vec3(-39.87 + x*WORLD_UNIT, 1+this->height, -39.87 + y*WORLD_UNIT);
}

void Food::draw(Shader* shader, float x, float y) {
  this->place(x, y);
  this->model->draw(shader);
}

void Food::drawArray(Shader* shader) {
  this->model->drawArray(shader, &this->Ms);
}
