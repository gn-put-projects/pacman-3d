#include "App.h"

int main() {
  App app;
  /*
  GLfloat lightAmbient[] = {1.0, 1.0, 1.0, 1.0};
  GLfloat lightDiffuse[] = {0.0, 0.0, 0.0, 1.0};
  GLfloat lightSpecular[] = {0.0, 0.0, 0.0, 1.0};
  GLfloat lightPosition[] = {0.0, 0.0, 0.0, 1.0};

  glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
  glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
  glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
  glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 0.5);
  glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.5);
  glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.5);
  glEnable(GL_LIGHT0);
  */
  app.loop();
  return 0;
}
