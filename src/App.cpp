#include "App.h"

#include <typeinfo>

void checkForErrors() {
  std::cout<<glGetError()<<"\n";
}

void windowResizeCallback(GLFWwindow* window, int width, int height) {
  if (height==0) return;
  //aspectRatio=(float)width/(float)height;
  glViewport(0, 0, width, height);
}

void errorCallbackMain(int error, const char* description) {
	fputs(description, stderr);
}

void keyCallbackMain(GLFWwindow* window, int key,
		      int scancode, int action, int mods) {
  App* self = (App*) glfwGetWindowUserPointer(window);
  self->keyCallback(window, key, scancode, action, mods);
}

void App::keyCallback(GLFWwindow* window, int key,
		      int scancode, int action, int mods) {
  if (action == GLFW_PRESS) {
    if (key == GLFW_KEY_X) {
      this->abort("X key pressed");
      return;
    }
    this->game->keyDown(key);
    return;
  }
  if (action == GLFW_RELEASE) {
    this->game->keyUp(key);
  }
}

App::App() {
  this->window = nullptr;
  this->shader = nullptr;
  this->game = nullptr;
  this->success = true;
  this->failureMsg = "";

  glfwSetErrorCallback(errorCallbackMain);
  
  if (!glfwInit()) {
    this->abort("Failed to initialize GLFW");
    return;
  }
  
  //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  ///glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  
  this->window = glfwCreateWindow(WINDOW_SIZE_X, WINDOW_SIZE_Y,
				  "Pacman 3D", NULL, NULL);

  if (this->window == nullptr) {
    this->abort("Failed to create GLFW window");
    return;
  }

  glfwSetWindowUserPointer(this->window, this);
 
  glfwMakeContextCurrent(this->window);
  
  if (!gladLoadGL()) {
    this->abort("Failed to initialize OpenGL context with Glad");
    return;
  }

  glfwSwapInterval(1);

  //glViewport(0, 0, WINDOW_SIZE_X, WINDOW_SIZE_Y);
  glEnable(GL_DEPTH_TEST);
  glClearColor(0.1, 0.1, 0.2, 1);
  
  glfwSetWindowSizeCallback(window, windowResizeCallback);
  glfwSetKeyCallback(window, keyCallbackMain);

  //this->shader = new Shader("src/shaders/vertex.glsl", "src/shaders/fragment.glsl");

  //this->shader = new Shader("src/shaders/v_lambert.glsl", "src/shaders/f_lambert.glsl");

  this->shader = new Shader("src/shaders/v_diffuse.glsl", "src/shaders/f_diffuse.glsl");

  if (this->shader->error) {
    this->abort("Failed to initialize shaders");
    return;
  }
    
  this->game = new Game(this->window);

  if (!this->game->success) {
    this->abort(this->game->failureMsg);
  }
}

App::~App() {
  if (this->shader)
    delete this->shader;
  if (this->game)
    delete this->game;
  glfwDestroyWindow(this->window);
  glfwTerminate();
}

void App::draw() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  this->game->render(this->shader);
  
  glfwSwapBuffers(this->window);
}

void App::loop() {
  while (!glfwWindowShouldClose(this->window)) {
    if (this->checkAbort()) return;
    glfwSetTime(0);
    this->draw();
    glfwPollEvents();
    this->game->update(glfwGetTime());
  }
}

void App::abort(string msg) {
  this->success = false;
  this->failureMsg = msg;
}

bool App::checkAbort() {
  if (this->success) return false;
  std::cout<<"Pacman 3D aborted: "<<this->failureMsg<<"\n";
  return true;
}
