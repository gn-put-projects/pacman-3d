#ifndef FOOD_H
#define FOOD_H

#include <cstdio>
#include <iostream>
#include <string>
#include <unordered_map>
#include <set>
#include <vector>

class Food;

#include "Game.h"
#include "Model.h"

using std::string;
using std::vector;

#define START_FOOD_COUNT 171
#define POS_ENCODE 25

#define MOVE_UNIT 100.0

#define WORLD_UNIT 0.038

#define LEVITATE_SPEED 2
#define LEVITATE_RANGE 0.4

#define WAVE_LENGTH 10

#define SUPERFOOD_SCALE 3.0f

#define PI 3.14159

class Food {
private:
  Model* model;
  Game* game;
  std::unordered_map<int, glm::mat4> Ms;
  void place(float, float);
  float height;
  float timePassed;
public:
  bool success;
  string failureMsg;
  bool eatFrom(int X, int Y);
  void update(float);
  void draw(Shader*, float, float);
  void drawArray(Shader*);
  Food(Game*, vector<glm::vec2>&);
  ~Food();
};

#endif
